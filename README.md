# NLP with Disaster Tweets (MVI semestral work)

[Kaggle competition](https://www.kaggle.com/competitions/nlp-getting-started/overview)

This case study compares MPNet, USE and InferSent models capable of creating sentence embeddings for the task of classifying disastrous tweets. 

To download InferSent model (must be executed in the root of this repository):

```
mkdir infersent
cd infersent
mkdir GloVe
curl -Lo GloVe/glove.840B.300d.zip http://nlp.stanford.edu/data/glove.840B.300d.zip
unzip GloVe/glove.840B.300d.zip -d GloVe/
mkdir fastText
curl -Lo fastText/crawl-300d-2M.vec.zip https://dl.fbaipublicfiles.com/fasttext/vectors-english/crawl-300d-2M.vec.zip
unzip fastText/crawl-300d-2M.vec.zip -d fastText/
```

